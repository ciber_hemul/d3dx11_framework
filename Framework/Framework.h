//
// Created by Andrey Gostev on 03.02.2022.
//

#pragma once

#ifndef D3DX11_FRAMEWORK_FRAMEWORK_H
#define D3DX11_FRAMEWORK_FRAMEWORK_H

#include "../common.h"
#include "../Util/Log.h"
#include "../Render/Window.h"
#include "../Input/InputMgr.h"
#include "../Render/Render.h"

namespace D3DX11_Framework {
    class Framework {
    private:
        bool handler();

        Window* m_wnd{nullptr};

        Render* m_render{nullptr};

        InputMgr* m_handler{nullptr};

        bool m_init{false};

    public:

        ~Framework();

        bool Init(DescWindow& descWindow, UINT style, int hIconRes, int hCursorRes, HBRUSH hBrush, int menuNameRes, int hIconSmallRes, WNDPROC userWndProc);

        void Run();

        void Close();

        void SetRender(Render* render);

        void AddInputListener(InputListener* listener);
    };
}

#endif //D3DX11_FRAMEWORK_FRAMEWORK_H
