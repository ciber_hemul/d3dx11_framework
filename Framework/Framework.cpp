//
// Created by Andrey Gostev on 03.02.2022.
//

#include "Framework.h"

namespace D3DX11_Framework {

    bool Framework::Init(DescWindow& descWindow, UINT style, int hIconRes, int hCursorRes, HBRUSH hBrush, int menuNameRes, int hIconSmallRes, WNDPROC userWndProc) {
        this->m_wnd = Window::Instance();
        this->m_handler = new InputMgr;

        if (!this->m_wnd || !this->m_handler) {
            Log::Instance()->Error("Fail to alloc memory"_cc);
            return false;
        }

        if (!this->m_wnd->Create(descWindow, style, hIconRes, hCursorRes, hBrush, menuNameRes, hIconSmallRes, userWndProc)) {
            Log::Instance()->Error("Fail to create window"_cc);
            return false;
        }

        this->m_wnd->SetInputHandler(this->m_handler);

        if (!this->m_render->CreateDevice(this->m_wnd->GetHWND())) {
            Log::Instance()->Error("Fail to create render"_cc);
            return false;
        }

        this->m_init = true;
        return true;
    }

    Framework::~Framework() {}

    void Framework::Run() {
        if (this->m_init) {
            while (this->handler()){}
        }
    }

    void Framework::Close() {
        this->m_init = false;
        _close(this->m_handler);
        _close(this->m_wnd);
        this->m_render->Release();
    }

    void Framework::SetRender(Render *render) {
        this->m_render = render;
    }

    void Framework::AddInputListener(InputListener *listener) {
        if (this->m_init) {
            this->m_handler->AddEventListener(listener);
        }
    }

    bool Framework::handler() {
        this->m_wnd->RunEventHandler();

        if (this->m_wnd->isExit()) {
            return false;
        }

        if (!this->m_wnd->isActive()) {
            return true;
        }

        if (this->m_wnd->isResize()) {}

        this->m_render->BeginFrame();
        if (!this->m_render->Draw()) {
            return false;
        }
        this->m_render->EndFrame();

        return true;
    }
}