//
// Created by Andrey Gostev on 04.02.2022.
//

#include "InputListener.h"

namespace D3DX11_Framework {

    MouseEvent::MouseEvent(POINT pos): pos(pos) {}

    MouseEventClick::MouseEventClick(mouseCode btn, POINT pos): MouseEvent(pos), btn(btn) {}

    MouseEventWheel::MouseEventWheel(int nWheel, POINT pos) : MouseEvent(pos), wheel(nWheel) {}

    KeyEvent::KeyEvent(wchar_t wc, keyCode code): wc(wc), code(code) {}
}
