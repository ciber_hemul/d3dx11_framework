//
// Created by Andrey Gostev on 04.02.2022.
//

#include "InputMgr.h"

namespace D3DX11_Framework {
    bool operator== (POINT first, POINT second) {
        return first.x == second.x && first.y == second.y;
    }

    InputMgr::InputMgr() {
        this->m_Listeners = {};
        this->m_cursorPosition = {};
        this->m_mouseWheel = 0;
        this->m_windowRect = {};

#ifdef _DEBUG
        Log::Instance()->Debug("InputMgr initialized"_cc);
#else
        Log::Instance()->Print("InputMgr initialized"_cc);
#endif
    }

    void InputMgr::Close() {
        this->m_Listeners.clear();

#ifdef _DEBUG
        Log::Instance()->Debug("InputMgr released"_cc);
#else
        Log::Instance()->Print("InputMgr released"_cc);
#endif
    }

    void InputMgr::Run(const UINT& msg, WPARAM wParam, LPARAM lParam) {
        if (this->m_Listeners.empty()) {
#ifdef _DEBUG
            Log::Instance()->Debug("No listeners found"_cc);
#else
            Log::Instance()->Print("No listeners found"_cc);
#endif
            return;
        }

        keyCode keyIndex;
        wchar_t buffer;
        BYTE lpKeyState[256];

        this->eventCursor();

        switch (msg) {
            case WM_KEYDOWN:
                keyIndex = static_cast<keyCode>(wParam);
                GetKeyboardState(lpKeyState);
                ToUnicode(wParam, HIWORD(lParam), lpKeyState, &buffer, 1, 0);
                this->eventKey(keyIndex, buffer, true);
                break;
            case WM_KEYUP:
                keyIndex = static_cast<keyCode>(wParam);
                GetKeyboardState(lpKeyState);
                ToUnicode(wParam, HIWORD(lParam), lpKeyState, &buffer, 1, 0);
                this->eventKey(keyIndex, buffer, false);
                break;
            case WM_LBUTTONDOWN:
                this->eventMouse(mouseCode::MOUSE_LEFT, true);
                break;
            case WM_LBUTTONUP:
                this->eventMouse(mouseCode::MOUSE_LEFT, false);
                break;
            case WM_RBUTTONDOWN:
                this->eventMouse(mouseCode::MOUSE_RIGHT, true);
                break;
            case WM_RBUTTONUP:
                this->eventMouse(mouseCode::MOUSE_RIGHT, false);
                break;
            case WM_MBUTTONDOWN:
                this->eventMouse(mouseCode::MOUSE_MIDDLE, true);
                break;
            case WM_MBUTTONUP:
                this->eventMouse(mouseCode::MOUSE_MIDDLE, false);
                break;
            case WM_MOUSEWHEEL:
                this->mouseWheel(GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA);
                break;
            default:
                break;
        }
    }

    void InputMgr::AddEventListener(InputListener* listener) {
        this->m_Listeners.emplace_back(listener);
    }

    void InputMgr::SetWindowRect(const RECT& windowRect) {
        this->m_windowRect = windowRect;
    }

    void InputMgr::eventCursor() {
        POINT pos;
        GetCursorPos(&pos);

        pos.x -= this->m_windowRect.left;
        pos.y -= this->m_windowRect.top;

        if (pos == this->m_cursorPosition) {
            return;
        }

        this->m_cursorPosition = pos;

        for (auto& el : this->m_Listeners) {
            if (!el) {
                continue;
            }
            if (el->MouseMove(MouseEvent(pos))) {
                return;
            }
        }
    }

    void InputMgr::eventMouse(const mouseCode code, bool press) {
        for (auto& el : this->m_Listeners) {
            if (!el) {
                continue;
            }

            if (press ? el->MousePressed(MouseEventClick(code, this->m_cursorPosition)) : el->MouseReleased(MouseEventClick(code, this->m_cursorPosition)))  {
                return;
            }
        }
    }

    void InputMgr::mouseWheel(int nWheel) {
        if (this->m_mouseWheel == nWheel) {
            return;
        }

        this->m_mouseWheel = nWheel;

        for (auto& el : this->m_Listeners) {
            if (!el) {
                continue;
            }

            if (el->MouseWheel(MouseEventWheel(this->m_mouseWheel, this->m_cursorPosition))) {
                return;
            }
        }
    }

    void InputMgr::eventKey(const keyCode code, const wchar_t ch, bool press) {
        for (auto& el : this->m_Listeners) {
            if (!el) {
                continue;
            }

            if (press ? el->KeyPressed(KeyEvent(ch, code)) : el->KeyReleased(KeyEvent(ch, code))) {
                return;
            }
        }
    }
}