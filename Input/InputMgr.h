//
// Created by Andrey Gostev on 04.02.2022.
//

#pragma once

#ifndef D3DX11_FRAMEWORK_INPUTMGR_H
#define D3DX11_FRAMEWORK_INPUTMGR_H

#include "InputListener.h"
#include "../Util/Log.h"


namespace D3DX11_Framework {

    class InputMgr {
    public:
        InputMgr();

        void Run(const UINT& msg, WPARAM wParam, LPARAM lParam);

        void AddEventListener(InputListener* listener);

        void SetWindowRect(const RECT& windowRect);

        void Close();

    private:
        void eventCursor();

        void eventMouse(mouseCode code, bool press);

        void mouseWheel(int nWheel);

        void eventKey(keyCode code, wchar_t ch, bool press);

        vector<InputListener*> m_Listeners;

        RECT m_windowRect{};
        POINT m_cursorPosition{};
        int m_mouseWheel{};

        friend bool operator== (POINT first, POINT second);
    };
}

#endif //D3DX11_FRAMEWORK_INPUTMGR_H
