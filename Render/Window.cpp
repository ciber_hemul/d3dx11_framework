//
// Created by Andrey Gostev on 05.02.2022.
//

#include "Window.h"

namespace D3DX11_Framework {

    DescWindow::DescWindow(int posX, int posY, int width, int height, char *caption, bool resizable): WindowPosition(posX, posY, width, height), caption(std::move(caption)), resizable(resizable) {}

    WindowPosition::WindowPosition(int posX, int posY, int width, int height):
            posX(posX), posY(posY), width(width), height(height){}

    Window *Window::Instance() {
        static Window instance;
        return &instance;
    }

    Window::Window() {
#ifdef _DEBUG
        Log::Instance()->Debug("Window initialized"_cc);
#else
        Log::Instance()->Print("Window initialized"_cc);
#endif
    }

    bool Window::Create(const DescWindow &descWnd, UINT style, int hIconRes, int hCursorRes, HBRUSH hBrush, int menuNameRes, int hIconSmallRes, WNDPROC userWndProc) {
        this->m_desc = descWnd;
        this->m_userProc = userWndProc;

        HINSTANCE hInst = GetModuleHandle(nullptr);

        HICON hIcon = LoadIconA(hInst, MAKEINTRESOURCEA(hIconRes));
        hIcon = hIcon ? hIcon : LoadIcon(NULL, IDI_WINLOGO);

        HCURSOR hCursor = LoadCursorA(hInst, MAKEINTRESOURCEA(hCursorRes));
        hCursor = hCursor ? hCursor : LoadCursor(NULL, IDC_ARROW);

        HICON hIconSmall = LoadIconA(hInst, MAKEINTRESOURCEA(hIconSmallRes));
        hIconSmall = hIconSmall ? hIconSmall : LoadIcon(NULL, IDI_WINLOGO);

        LPCSTR menuName = MAKEINTRESOURCEA(menuNameRes);

        this->m_wnd.cbSize = sizeof(WNDCLASSEXA);
        this->m_wnd.style = style;
        this->m_wnd.lpfnWndProc = StaticWndProc;
        this->m_wnd.cbClsExtra = 0;
        this->m_wnd.cbWndExtra = 0;
        this->m_wnd.hInstance = hInst;
        this->m_wnd.hIcon = hIcon;
        this->m_wnd.hCursor = hCursor;
        this->m_wnd.hbrBackground = hBrush;
        this->m_wnd.lpszMenuName = menuName;
        this->m_wnd.lpszClassName = this->m_desc.caption;
        this->m_wnd.hIconSm = hIconSmall;

        if (!RegisterClassExA(&this->m_wnd)) {
            Log::Instance()->Error("Fail to register window"_cc);
            return false;
        }

        RECT rect = {this->m_desc.posX, this->m_desc.posY, this->m_desc.width + this->m_desc.posX, this->m_desc.height + this->m_desc.posY};
        AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW | WS_VISIBLE, (bool)menuName);

        this->m_desc.posX = rect.left;
        this->m_desc.posY = rect.top;
        this->m_desc.width = rect.right - rect.left;
        this->m_desc.height = rect.bottom - rect.top;

        this->m_hWnd = CreateWindowExA(NULL, this->m_desc.caption, this->m_desc.caption, WS_VISIBLE | WS_OVERLAPPEDWINDOW, this->m_desc.posX, this->m_desc.posY, this->m_desc.width, this->m_desc.height, nullptr, nullptr, hInst, nullptr);

        if (!this->m_hWnd) {
            Log::Instance()->Error("Fail to create window"_cc);
            return false;
        }

        ShowWindow(this->m_hWnd, SW_SHOW);
        UpdateWindow(this->m_hWnd);

        return true;
    }

    void Window::Close() {
        if (this->m_hWnd) {
            DestroyWindow(this->m_hWnd);
        }

#ifdef _DEBUG
        Log::Instance()->Debug("Window destroyed"_cc);
#else
        Log::Instance()->Print("Window destroyed"_cc);
#endif
    }

    void Window::RunEventHandler() {
        MSG msg{nullptr};

        while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    void Window::SetInputHandler(InputMgr *mgr) {
        this->m_inputMgr = mgr;
        this->UpdateWindowState();
    }

    DescWindow Window::GetWindowInfo() const {
        return this->m_desc;
    }

    HWND Window::GetHWND() const {
        return this->m_hWnd;
    }

    bool Window::isExit() const {
        return this->m_isExit;
    }

    bool Window::isActive() const {
        return this->m_isActive;
    }

    bool Window::isResize() {
        bool isRes = this->m_isResize;
        this->m_isResize = false;
        return isRes;
    }

    LRESULT Window::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
        switch (msg) {
            case WM_CREATE:
                return 0;
            case WM_CLOSE:
                this->m_isExit = true;
                return 0;
            case WM_ACTIVATE:
                this->m_isActive = LOWORD(wParam) != WA_INACTIVE;
                return 0;
            case WM_MOVE:
                this->m_desc.posX = LOWORD(lParam);
                this->m_desc.posY = HIWORD(lParam);
                this->UpdateWindowState();
                return 0;
            case WM_SIZE:
                if (!this->m_desc.resizable) {
                    return 0;
                }

                this->m_desc.width = LOWORD(lParam);
                this->m_desc.height = HIWORD(lParam);

                this->m_isResize = true;

                if (wParam == SIZE_MINIMIZED) {
                    this->m_isActive = false;
                    this->m_isMinimized = true;
                    this->m_isMaximized = false;
                }

                if (wParam == SIZE_MAXIMIZED) {
                    this->m_isActive = true;
                    this->m_isMinimized = false;
                    this->m_isMaximized = true;
                }

                if (wParam == SIZE_RESTORED) {
                    if (this->m_isMinimized) {
                        this->m_isActive = false;
                        this->m_isMinimized = true;
                    }
                    elif (this->m_isMaximized) {
                        this->m_isActive = true;
                        this->m_isMaximized = true;
                    }
                }

                this->UpdateWindowState();
                return 0;

            case WM_MOUSEMOVE:
            case WM_LBUTTONUP:
            case WM_LBUTTONDOWN:
            case WM_RBUTTONUP:
            case WM_RBUTTONDOWN:
            case WM_MBUTTONUP:
            case WM_MBUTTONDOWN:
            case WM_MOUSEWHEEL:
            case WM_KEYUP:
            case WM_KEYDOWN:
                if (this->m_inputMgr) {
                    this->m_inputMgr->Run(msg, wParam, lParam);
                }
                return 0;
            default:
                if (this->m_userProc) {
                    return this->m_userProc(hWnd, msg, wParam, lParam);
                }
                return DefWindowProcW(hWnd, msg, wParam, lParam);
        }
    }

    void Window::UpdateWindowState() {
        if (this->m_inputMgr) {
            this->m_inputMgr->SetWindowRect({this->m_desc.posX, this->m_desc.posY, this->m_desc.width + this->m_desc.posX, this->m_desc.height + this->m_desc.posY});
        }
    }

    LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
        return Window::Instance()->WndProc(hWnd, msg, wParam, lParam);
    }
}