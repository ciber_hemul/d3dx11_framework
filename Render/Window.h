//
// Created by Andrey Gostev on 05.02.2022.
//

#pragma once

#ifndef D3DX11_FRAMEWORK_WINDOW_H
#define D3DX11_FRAMEWORK_WINDOW_H

#include "../Util/Log.h"
#include "../Input/InputMgr.h"

namespace D3DX11_Framework {

    struct WindowPosition {
        WindowPosition(int posX, int posY, int width, int height);

        int posX;
        int posY;
        int width;
        int height;
    };

    struct DescWindow : public WindowPosition {
        explicit DescWindow(int posX = 200, int posY = 200, int width = 640, int height = 480, char *caption = "DirectX"_cc, bool resizable = true);

        char* caption = "DirectX"_cc;
        bool resizable{true};
    };

    class Window {
    public:
        static Window* Instance();

        bool Create(const DescWindow& descWnd, UINT style, int hIconRes, int hCursorRes, HBRUSH hBrush, int menuNameRes, int hIconSmallRes, WNDPROC userWndProc);

        void Close();

        void RunEventHandler();

        void SetInputHandler(InputMgr* mgr);

        [[nodiscard]] DescWindow GetWindowInfo() const;

        [[nodiscard]] HWND GetHWND() const;

        [[nodiscard]] bool isExit() const;

        [[nodiscard]] bool isActive() const;

        bool isResize();

        LRESULT WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    private:
        Window();

        void UpdateWindowState();

        DescWindow m_desc;
        InputMgr* m_inputMgr{nullptr};
        HWND m_hWnd{nullptr};
        WNDCLASSEXA m_wnd{0};
        /*
         * User callback function to proceed system commands. Commands WM_CREATE, WM_CLOSE, WM_ACTIVATE, WM_MOVE, WM_SIZE, WM_MOUSEMOVE, WM_LBUTTONUP, WM_LBUTTONDOWN, WM_RBUTTONUP, WM_RBUTTONDOWN, WM_MBUTTONUP, WM_MBUTTONDOWN, WM_MOUSEWHEEL, WM_KEYUP, WM_KEYDOWN are reserved.
         */
        WNDPROC m_userProc;
        bool m_isExit{false};
        bool m_isActive{true};
        bool m_isMinimized{false};
        bool m_isMaximized{false};
        bool m_isResize{false};
    };

    static LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
}

#endif //D3DX11_FRAMEWORK_WINDOW_H
