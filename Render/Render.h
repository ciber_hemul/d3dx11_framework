//
// Created by Andrey Gostev on 07.02.2022.
//

#pragma once

#ifndef D3DX11_FRAMEWORK_RENDER_H
#define D3DX11_FRAMEWORK_RENDER_H

#include "../common.h"

namespace D3DX11_Framework {
    class Render {
    public:
        Render(void);

        virtual ~Render(void);

        bool CreateDevice(HWND hWnd);

        void BeginFrame();

        void EndFrame();

        void Release();

        virtual bool Init(HWND hWnd) = 0;

        virtual bool Draw(void) = 0;

        virtual void Close(void) = 0;

    protected:
        HRESULT CompileShaderFromFile(wchar_t* fileName, LPCSTR entryPoint, LPCSTR shaderModel, ID3DBlob** bOut, HWND errorWnd);

        D3D_DRIVER_TYPE m_dType;
        D3D_FEATURE_LEVEL m_fLevel;
        ID3D11Device* m_device;
        ID3D11DeviceContext* m_context;
        IDXGISwapChain* m_sChain;
        ID3D11RenderTargetView* m_tView;
        ID3D11DepthStencilView* m_depthView;
        ID3D11Texture2D* m_depthStencil;
    };
}

#endif //D3DX11_FRAMEWORK_RENDER_H
