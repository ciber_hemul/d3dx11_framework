//
// Created by Andrey Gostev on 07.02.2022.
//

#include "Render.h"

namespace D3DX11_Framework {

    Render::Render() {
        this->m_dType = D3D_DRIVER_TYPE_HARDWARE;
        this->m_fLevel = D3D_FEATURE_LEVEL_11_1;
        this->m_device = nullptr;
        this->m_context = nullptr;
        this->m_sChain = nullptr;
        this->m_tView = nullptr;
        this->m_depthView = nullptr;
        this->m_depthStencil = nullptr;
    }

    Render::~Render() {}

    bool Render::CreateDevice(HWND hWnd) {
        RECT rc;
        GetClientRect(hWnd, &rc);
        UINT width = rc.right - rc.left;
        UINT height = rc.bottom - rc.top;

        UINT cDeviceFlags = 0x0;
#ifdef _DEBUG
        cDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

        D3D_FEATURE_LEVEL fLevel[3] = {
                D3D_FEATURE_LEVEL_11_0,
                D3D_FEATURE_LEVEL_10_1,
                D3D_FEATURE_LEVEL_10_0

        };

        DXGI_SWAP_CHAIN_DESC swapChainDesc;

        ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

        swapChainDesc.BufferDesc.Width = width;
        swapChainDesc.BufferDesc.Height = height;
        swapChainDesc.BufferDesc.RefreshRate.Numerator = 61;
        swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
        swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE;
        swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_CENTERED;
        swapChainDesc.SampleDesc.Count = 1;
        swapChainDesc.SampleDesc.Quality = 0;
        swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        swapChainDesc.BufferCount = 1;
        swapChainDesc.OutputWindow = hWnd;
        swapChainDesc.Windowed = TRUE;
        swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
        swapChainDesc.Flags = 0;

        vector<D3D_DRIVER_TYPE> dType = {
                D3D_DRIVER_TYPE_HARDWARE,
                D3D_DRIVER_TYPE_WARP,
                D3D_DRIVER_TYPE_SOFTWARE
        };

        dType.emplace(dType.cbegin(), this->m_dType);

        HRESULT hr;
        for (auto &i : dType) {
            hr = D3D11CreateDeviceAndSwapChain(nullptr, i, nullptr, cDeviceFlags, fLevel, (UINT)3, D3D11_SDK_VERSION, &swapChainDesc, &this->m_sChain, &this->m_device, &this->m_fLevel, &this->m_context);
            if(SUCCEEDED(hr)) {
                break;
            }
        }
        if(FAILED(hr)) {
            return false;
        }

        ID3D11Texture2D* bBuffer = nullptr;

        if (FAILED(this->m_sChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&bBuffer))) {
            return false;
        }

        if (FAILED(this->m_device->CreateRenderTargetView(bBuffer, nullptr, &this->m_tView))) {
            _release(bBuffer);
            return false;
        }
        _release(bBuffer);

        D3D11_TEXTURE2D_DESC depthDesc;

        ZeroMemory(&depthDesc, sizeof(depthDesc));

        depthDesc.Width = width;
        depthDesc.Height = height;
        depthDesc.MipLevels = 1;
        depthDesc.ArraySize = 1;
        depthDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
        depthDesc.SampleDesc.Count = 1;
        depthDesc.SampleDesc.Quality = 0;
        depthDesc.Usage = D3D11_USAGE_DEFAULT;
        depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
        depthDesc.CPUAccessFlags = 0;
        depthDesc.MiscFlags = 0;

        if (FAILED(this->m_device->CreateTexture2D(&depthDesc, 0, &this->m_depthStencil))) {
            return false;
        }

        D3D11_DEPTH_STENCIL_VIEW_DESC depthViewDesc;

        ZeroMemory(&depthViewDesc, sizeof(depthViewDesc));

        depthViewDesc.Format = depthDesc.Format;
        depthViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
        depthViewDesc.Texture2D.MipSlice = 0;

        HRESULT r = this->m_device->CreateDepthStencilView(this->m_depthStencil, &depthViewDesc, &this->m_depthView);
        if (FAILED(r)) {
            return false;
        }

        this->m_context->OMSetRenderTargets(1, &this->m_tView, this->m_depthView);

        D3D11_VIEWPORT vPort;

        vPort.TopLeftX = 0;
        vPort.TopLeftY = 0;
        vPort.Width = (float)width;
        vPort.Height = (float)height;
        vPort.MinDepth = 0.0f;
        vPort.MaxDepth = 1.0f;

        this->m_context->RSSetViewports(1, &vPort);

        return this->Init(hWnd);
    }

    void Render::BeginFrame() {
        float cColor[4] = {0.0f, 0.125f, 0.3f, 0.0f};

        this->m_context->ClearRenderTargetView(this->m_tView, cColor);
        this->m_context->ClearDepthStencilView(this->m_depthView, D3D11_CLEAR_DEPTH, 1.0f, 0);
    }

    void Render::EndFrame() {
        this->m_sChain->Present(1, 0);
    }

    void Render::Release() {
        Close();

        _release(this->m_context);
        _release(this->m_sChain);
        _release(this->m_tView);
        _release(this->m_device);
        _release(this->m_depthView);
        _release(this->m_depthStencil);
    }

    HRESULT Render::CompileShaderFromFile(wchar_t *fileName, LPCSTR entryPoint, LPCSTR shaderModel, ID3DBlob **bOut, HWND errorWnd) {
        ID3DBlob* errMsg{};

        UINT compileFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
        compileFlags |= D3DCOMPILE_DEBUG;
#endif

        HRESULT result = D3DCompileFromFile(fileName, nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, entryPoint, shaderModel, compileFlags, NULL, bOut, &errMsg);

        if (FAILED(result) && errMsg) {
            MessageBoxEx(errorWnd, (char*)errMsg->GetBufferPointer(), "Compile Error"_cc, MB_OK, MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL));
#ifdef _DEBUG
            OutputDebugString((char*)errMsg->GetBufferPointer());
#endif
        }
        _release(errMsg);

        return result;
    }
}