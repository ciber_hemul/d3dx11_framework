//
// Created by Andrey Gostev on 03.02.2022.
//

#include "common.h"

char* operator"" _cc(const char* s, size_t size) {
    return const_cast<char*>(s);
}

wchar_t* operator"" _wc(const wchar_t *s, size_t size) {
    return const_cast<wchar_t*>(s);
}

