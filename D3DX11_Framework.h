#ifndef D3DX11_FRAMEWORK_LIBRARY_H
#define D3DX11_FRAMEWORK_LIBRARY_H

#pragma once

#include "common.h"
#include "Render/Render.h"
#include "Input/InputListener.h"
#include "Framework/Framework.h"

#endif //D3DX11_FRAMEWORK_LIBRARY_H
