cmake_minimum_required(VERSION 3.21)

project (D3DX11_Framework)

set(CMAKE_CXX_STANDARD 23)

include_directories("C:/Program Files (x86)/Boost")

add_library(D3DX11_Framework common.h Framework/Framework.cpp Framework/Framework.h Util/Log.cpp Util/Log.h common.cpp Input/InputListener.cpp Input/InputListener.h Input/InputMgr.cpp Input/InputMgr.h Render/Window.cpp Render/Window.h Render/Render.cpp Render/Render.h)
