//
// Created by Andrey Gostev on 03.02.2022.
//

#pragma once

#ifndef D3DX11_FRAMEWORK_COMMON_H
#define D3DX11_FRAMEWORK_COMMON_H

#ifdef _DEBUG
#define _ITERATOR_DEBUG_LEVEL 2
#endif

#include <iostream>
#include <chrono>
#include <string>
#include <vector>
#include <fstream>
#include <utility>

#define WIN32_LEAN_AND_MEAN
#define elif else if

#include <Windows.h>
#include <dwmapi.h>

#include <d3dcompiler.h>
#include <d3d11.h>
#include <boost/date_time/posix_time/posix_time.hpp>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dwmapi.lib")

using namespace std;
using namespace boost::posix_time;

//remove "const" qualifier
char* operator"" _cc(const char* s, size_t size);

wchar_t* operator"" _wc(const wchar_t* s, size_t size);

#define _delete(pointer) {if(pointer){delete pointer;pointer=nullptr;}};

#define _deleteArray(pointer) {if(pointer){delete [] pointer;pointer=nullptr;}};

#define _close(pointer) {if(pointer){pointer->Close();pointer=nullptr;}};

#define _release(pointer) {if(pointer){(pointer)->Release();(pointer)=nullptr;}};

#endif  //D3DX11_FRAMEWORK_COMMON_H