//
// Created by Andrey Gostev on 03.02.2022.
//

#include "Log.h"

namespace D3DX11_Framework {
    Log::Log() {
        this->m_file.open(DEFAULT_LOGNAME, ios::out);

        if (this->m_file.fail()) {
#ifdef _DEBUG
            cout << "Fail to open log!\n";
#endif
            return;
        }

        this->m_file << "Log created: " << second_clock::local_time() << "\n\n";
        this->m_file << setw(40) << setfill('-') << "\n";
    }

    Log::~Log() {
        if (this->m_file.fail()) {
#ifdef _DEBUG
            cout << "Fail to close non-existent log!\n";
#endif
            return;
        }

        this->m_file << setw(40) << setfill('-') << "\n\n";
        this->m_file << "Log released: " << second_clock::local_time() << "\n";

        this->m_file.close();
    }

    Log* Log::Instance() {
        static Log instance;
        return &instance;
    }

    void Log::m_print(const string& type, const string& msg) {
        string out = format("{} [{}] {}{}", boost::lexical_cast<string>(second_clock::local_time()), type, msg, "\n");
        this->m_file << out;
#ifdef _DEBUG
        cout << out;
#endif
    }

#ifdef _DEBUG
    void Log::Debug(char* msg, ...) {
        va_list args;
                va_start(args, msg);
        unsigned len = _vscprintf(msg, args) + 1;
        char* result = new char[len];
        vsprintf_s(result, len, msg, args);
        this->m_print("DEBUG", result);
                va_end(args);
        delete [] result;
    }
#endif

    void Log::Print(char* msg, ...) {
        va_list args;
                va_start(args, msg);
        unsigned len = _vscprintf(msg, args) + 1;
        char* result = new char[len];
        vsprintf_s(result, len, msg, args);
        this->m_print("NOTICE", result);
                va_end(args);
        delete [] result;
    }

    void Log::Error(char* msg, ...) {
        va_list args;
                va_start(args, msg);
        unsigned len = _vscprintf(msg, args) + 1;
        char* result = new char[len];
        vsprintf_s(result, len, msg, args);
        this->m_print("ERROR", result);
                va_end(args);
        delete [] result;
    }
}
