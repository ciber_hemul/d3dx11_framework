//
// Created by Andrey Gostev on 03.02.2022.
//

#pragma once

#ifndef D3DX11_FRAMEWORK_LOG_H
#define D3DX11_FRAMEWORK_LOG_H

#include "../common.h"

#define DEFAULT_LOGNAME "log.txt"

namespace D3DX11_Framework {
    class Log {
    private:
        fstream m_file;

        Log();

        void m_print(const string& type, const string& msg);

    public:
        ~Log();

        static Log* Instance();

#ifdef _DEBUG
        void Debug(char* msg, ...);
#endif

        void Print(char* msg, ...);

        void Error(char* msg, ...);
    };
}
#endif //D3DX11_FRAMEWORK_LOG_H
